<?php

use App\User;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class {{class}} extends Seeder
{
    public function run()
    {
        User::truncate();
        // TestDummy::times(20)->create('App\Post');
        DB::table('users')->insert([

        'first_name'=>'Serdar',
        'last_name'=>'Türkmen',
        'username'=>'Serdar Türkmen',
        'username_slug'=>'serdar-turkmen',
        'email'=>'sturkmen@gmail.com',
        'comfirmation_code'=>md5(microtime()+env('APP_KEY')),
        'comfirmed'=>1,
        'settings'=>'[
               ''=>'',
               ''=>'',
               ''=>'',
               ''=>'',
               ''=>'',

        ]',
        'password'=>bcrypt('123456'),
    ]);
    }
}
